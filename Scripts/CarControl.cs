﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarControl : MonoBehaviour
{

    public float carSpeed;
    Vector3 position;

    void Start()
    {
        position = transform.position;
    }

   
    void Update()
    {
        position.y += Input.GetAxis("Vertical") * carSpeed * Time.deltaTime;
        position.x += Input.GetAxis ("Horizontal") * carSpeed * Time.deltaTime;

        position.x = Mathf.Clamp(position.x, -2f, 2f);
        position.y = Mathf.Clamp(position.y, -4f, 3f);

        transform.position = position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy Car")
        {
            Destroy(gameObject);
            SceneManager.LoadScene("menu");
        }
    }
}
