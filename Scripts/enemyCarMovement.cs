﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyCarMovement : MonoBehaviour
{

    public float speed = 5f;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Translate(new Vector3(0, 1, 0) * speed * Time.deltaTime);
        if (transform.position.y < -8)
        {
            Destroy(gameObject);
        }
    }
}
