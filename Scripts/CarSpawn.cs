﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawn : MonoBehaviour
{

    public GameObject[] cars;
    public int carNumber;
    float timer;

    void Start()
    {
        timer = Random.Range(0.5f, 1.5f);
    }

    
    void Update()
    {

        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            int lane = Random.Range(0, 4);
            carNumber = Random.Range(0, 2);
            if (lane == 0)
            {
                Vector3 carPosition = new Vector3(-1.9f, transform.position.y, transform.position.z);
                Instantiate(cars[carNumber], carPosition, transform.rotation);
            }
            if (lane == 1)
            {
                Vector3 carPosition = new Vector3(-0.65f, transform.position.y, transform.position.z);
                Instantiate(cars[carNumber], carPosition, transform.rotation);
            }
            if (lane == 2)
            {
                Vector3 carPosition = new Vector3(0.59f, transform.position.y, transform.position.z);
                Instantiate(cars[carNumber], carPosition, transform.rotation);
            }
            if(lane == 3)
            {
                Vector3 carPosition = new Vector3(1.89f, transform.position.y, transform.position.z);
                Instantiate(cars[carNumber], carPosition, transform.rotation);
            }
            timer = Random.Range(0.5f, 1.5f);
        }

        
    }
}
